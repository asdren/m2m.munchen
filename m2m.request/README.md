# One-time setup

```
docker volume create --name maven-repo
```

You only need to do this once on your machine. It creates a persistent volume that will hold all the Maven jars so you don't have to download them every time you run a build.

# To build a new JAR

```
docker run -it --rm -v maven-repo:/root/.m2 -v <path-to-project-dir>:/usr/src/mymaven -w /usr/src/mymaven maven:3.5.3-jdk-8-alpine mvn clean install
```

`<path-to-project-dir>` must be the fully-qualified path to the m2m.request directory.

On Windows, you just have to type it out.

On Macos or Linux, you can use `$(pwd)` to insert the current directory's path like this:

```
docker run -it --rm -v maven-repo:/root/.m2 -v $(pwd):/usr/src/mymaven -w /usr/src/mymaven maven:3.5.3-jdk-8-alpine mvn clean install
```

This uses one docker container to run Maven. That way we have a known version of Maven and Java, without any conflicts with your local machine. (I.e., we need to build with Java 8 but you might have Java 9 or 10 installed.)

This also uses a persistent volume to hold all the dependency jars that you need.

# To build a Docker image

First, build a JAR, then run:

```
docker build . -t m2m.request
```
