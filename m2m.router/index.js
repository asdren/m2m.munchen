const express = require("express")
const tracing = require("zipkin-javascript-opentracing")
const { BatchRecorder } = require("zipkin")
const { HttpLogger } = require("zipkin-transport-http")
const proxy = require("http-proxy-middleware")

const proxyPort = 1972

const tracer = new tracing({
  serviceName: "router",
  recorder: new BatchRecorder({
    logger: new HttpLogger({
      endpoint: process.env.ZIPKIN_BASE_URL + "/api/v1/spans"
    })
  }),
  kind: "client"
});

var proxyOptions = {
    onProxyReq: (proxyReq, req, res) => {
        const context = tracer.extract(
            tracing.FORMAT_HTTP_HEADERS,
            req.headers
        )

        span = tracer.startSpan(req.path, {childOf: context})

        var headers = {}
        tracer.inject(span, tracing.FORMAT_HTTP_HEADERS, headers)

        for (var k in headers) {
            proxyReq.setHeader(k, headers[k])
        }

        res.span = span
    },
    onError: (err, req, res) => {
        if (res.span) {
            res.span.setTag(opentracing.Tags.ERROR, true);
            res.span.log({'event': 'error', 'error.object': err, 'message': err.message, 'stack': err.stack});
            res.span.finish()
        }
    },
    onProxyRes: (proxyRes, req, res) => {
        if (res.span) {
            res.span.finish()
        }
    },
    changeOrigin: true,
    xfwd: true,
    target: process.env.LOBSTERS_DNS_NAME,
    router: {
        "/modlog" : process.env.MODLOG_DNS_NAME
    }
  changeOrigin: true,
  target: process.env.LOBSTERS_URL,
  router: {
    "/modlog": process.env.MODLOG_URL
  }
}

var app = express()
app.use(proxy("/", proxyOptions)).listen(proxyPort, () => console.log('Listening on port:' + proxyPort))
