package com.mhp.m2mws.requests

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(exported = false)
internal interface RequestRepository : PagingAndSortingRepository<Request, Long> {
    fun findByStatus(status: Request.Status): Iterable<Request>
}
