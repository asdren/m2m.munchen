package com.mhp.m2mws.requests

import javax.persistence.Entity

@Entity
class Notarization(
    val principal: String,
    val reason: String
) : EntityModel()
