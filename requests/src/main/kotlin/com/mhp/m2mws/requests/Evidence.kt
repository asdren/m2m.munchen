package com.mhp.m2mws.requests

import com.fasterxml.jackson.annotation.JsonIgnore

import javax.persistence.*

@Entity
class Evidence: EntityModel() {

    @JsonIgnore
    @ManyToOne
    var request: Request? = null

    var link: String? = null

    // Needed because "text" is a reserved word in SQL
    @Column(name = "`text`")
    var text: String? = null
}
