package com.mhp.m2mws.requests

import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/**
 * Abstract super class for all entities
 */
@MappedSuperclass
abstract class EntityModel {
    @Id
    @GeneratedValue
    var id: Long? = null
}
