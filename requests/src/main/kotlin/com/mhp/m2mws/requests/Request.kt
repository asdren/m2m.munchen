package com.mhp.m2mws.requests

import javax.persistence.*
import java.util.HashSet

@Entity
class Request(
    var subject: String,
    var `object`: String
) : EntityModel() {

    var status: Status = Status.PENDING

    @OneToOne(cascade = [(CascadeType.ALL)])
    var notarization: Notarization? = null

    @OneToMany(cascade = [(CascadeType.ALL)], mappedBy = "request", fetch = FetchType.EAGER)
    val evidence: Set<Evidence> = HashSet()

    fun isOpen() = status == Status.PENDING

    fun notarize(notarization: Notarization?): Request {
        this.notarization = notarization
        return this
    }

    enum class Status {
        PENDING, APPROVED, REJECTED
    }
}

