package com.mhp.m2mws.requests

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.Identifiable
import org.springframework.hateoas.Link
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestMapping

@RestController
@RequestMapping("/request")
class RequestController {

    @Autowired
    private lateinit var requestRepository: RequestRepository

    @GetMapping
    fun index(): Resources<Resource<Request>> = Resources(requestRepository.findAll().map {
        this.createResource(it)
    })

    @GetMapping(path = ["/{id}"])
    fun getById(@PathVariable id: Long): ResponseEntity<Resource<Request>> {
        val request = requestRepository.findById(id)
        return if (request.isPresent) {
            ResponseEntity.ok(this.createResource(request.get()))
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @GetMapping(path = ["search"])
    fun findByStatus(@RequestParam("status") param: Request.Status): Resources<Resource<Request>> {
        val byStatus = requestRepository.findByStatus(param)
        return Resources(byStatus.map {
            this.createResource(it)
        })
    }

    @PostMapping
    fun add(@RequestBody input: Request): ResponseEntity<Resource<Request>> {
        input.evidence.forEach {
            it.request = input
        }
        val entity = requestRepository.save(input)
        val resource = createResource(entity)
        return ResponseEntity.created(entity.linkTo()!!.toUri()).body(resource)
    }

    @PostMapping(path = ["/{id}/{action}"])
    fun action(@PathVariable(name = "id") id: Long,
               @PathVariable("action") action: Action,
               @RequestBody notarization: Notarization?): ResponseEntity<Resource<Request>> {
        val request = requestRepository.findById(id).orElse(null) ?: return ResponseEntity.notFound().build()
        return if (request.isOpen()) {
            action.performTo(request).notarize(notarization)
                .let {
                    requestRepository.save(it)
                }.let {
                    createResource(it)
                }.let {
                    ResponseEntity.ok(it)
                }
        } else ResponseEntity.status(403).build()
    }

    enum class Action(private val targetStatus: Request.Status) : Identifiable<String> {
        approve(Request.Status.APPROVED),
        reject(Request.Status.REJECTED);

        fun performTo(response: Request): Request {
            response.status = targetStatus
            return response
        }

        override fun getId(): String {
            return this.name
        }
    }

    private fun createResource(req: Request): Resource<Request> {
        val resource = Resource(req)
        addSelfLink(resource)
        addActionLinks(resource)
        return resource
    }

    private fun addSelfLink(resource: Resource<Request>) {
        resource.content.linkTo()?.let { link ->
            resource.add(link.withSelfRel())
        }
    }

    private fun addActionLinks(resource: Resource<Request>) {
        if (resource.content.isOpen()) {
            for (a in Action.values()) {
                val actionLink = createActionLink(a, resource)
                actionLink?.let {
                    resource.add(actionLink)
                }
            }
        }
    }

    private fun createActionLink(action: Action, resource: Resource<Request>): Link? {
        return resource.content.id?.let { requestId ->
            linkTo(methodOn(RequestController::class.java).action(requestId, action, null)).withRel(action.name)
        }
    }

    private fun Request.linkTo() = this.id?.let { linkTo(methodOn(RequestController::class.java).getById(it)) }
}
