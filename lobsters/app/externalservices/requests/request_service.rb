require 'hat_request_representer'
require 'faraday'
require 'zipkin-tracer'
require 'json'

class RequestService

  # @param [Object] object
  # @param [Object] subject
  # @param [Object] hat_request
  def self.create(object, subject, hat_request)
    text_evidence = { text: hat_request[:comment] }
    linked_evidence = { link: hat_request[:link] }

    # Make a data structure that will be the body of the API request
    hat_request = HatRequestRepresenter.new(
      subject: subject,
      object: object,
      evidence: [text_evidence, linked_evidence])

    # "Faraday" makes the HTTP call for us.
    conn = Faraday.new(url: Lobsters::Application.config.request_service_uri) do |c|
      c.use ZipkinTracer::FaradayHandler
      c.request :url_encoded
      c.adapter Faraday.default_adapter
    end

    # Do the call to the API
    conn.post do |req|
      req.headers['Content-Type'] = 'application/json'
      req.body = hat_request.to_json
    end
  end

  def self.list(status)
    # "Faraday" makes the HTTP call for us.
    conn = Faraday.new(url: Lobsters::Application.config.request_service_uri + '/search') {|c|
      c.use ZipkinTracer::FaradayHandler
      c.request :url_encoded
      c.adapter Faraday.default_adapter
    }

    # Do the call to the API
    response = conn.get do |req|
      req.params = { status: status }
      req.headers['Content-Type'] = 'application/json'
    end


    JSON.parse(response.body)
  end

  def self.countBy(status)
    list(status).size
  end

end
